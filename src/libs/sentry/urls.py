from django.conf.urls import url
from . import views


app_name = 'sentry'
urlpatterns = [
    url(r'^tunnel/$', view=views.tunnel, name='tunnel'),
]
