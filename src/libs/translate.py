from django import http


class RedirectMiddleware(object):

    @staticmethod
    def process_request(request):

        full_path = request.get_full_path()
        if '/media/' in full_path:
            return False

        lang = request.COOKIES.get('lang', None)
        if lang is None:
            return False

        if lang == 'es':
            return http.HttpResponseRedirect('https://translate.google.ru/translate?sl=en&tl=es&u={0}'.format(request.build_absolute_uri()))
        else:
            return False