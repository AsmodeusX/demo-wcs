from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings


def domain(request):
    site = get_current_site(request)
    return {
        'domain': site.domain,
    }


def company(request):
    return {
        'COMPANY': settings.PROJECT_NAME,
    }


def google_recaptcha_public_key(request):
    return {
        'GOOGLE_RECAPTCHA_PUBLIC_KEY': settings.GOOGLE_RECAPTCHA_PUBLIC_KEY,
    }
