(function($) {

    $(window).on('load', function () {
        checkHash();
    });

    function checkHash() {
        var hash = window.location.hash.toLowerCase(); //Нашли все что в хэше
        var format_hash = hash.replace('#', ''); // Убрали #, оставили только название блока
        if (!!format_hash) { //Если есть название блока
            var $elem = $("[data-hash='" + format_hash + "']").eq(0); // То пытаемся найти первый блок с подходящим названием
            if ($elem && $elem.length > 0) { // Если блок найден
                if ($elem.closest('.block--faq')) {
                    setTimeout(function () {

                        var tab = $elem.closest('.questions').data('tab');
                        var $tab = $('.tab[data-tab="' + tab + '"]');
                        if (!$tab.hasClass('active')) {
                            $tab.find('span').trigger('click');
                        }
                        $elem.find('.question__question').trigger('click');
                        $.scrollTo($elem.offset().top - $('.header--fixed').innerHeight() + 1, 2000); //То скроллим до него - высота фиксированного хэдера
                    },500);
                } else {
                    $.scrollTo($elem.offset().top - $('.header--fixed').innerHeight() + 1, 2000); //То скроллим до него - высота фиксированного хэдера
                }
            } else { // Если блок не найден
                $elem = $("[data-slug='" + format_hash +"']"); //То ищем элемент, открывающий попап со slug, аналогичным хэшу
                if ($elem && $elem.length > 0) { // Если такой элемент найден
                    $.scrollTo($elem.offset().top - $('.header--fixed').innerHeight() + 1, 2000); // То скролим до него - высота фиксированного хэдера
                    setTimeout(function () {
                        $elem.trigger('click'); //И программно кликаем по нему
                    }, 2000)
                }
            }
        }
    }

})(jQuery);
