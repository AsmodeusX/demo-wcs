# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2021-09-09 06:42
from __future__ import unicode_literals

from django.db import migrations, models
import libs.stdimage.fields
import libs.storages.media_storage


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MainPageConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('header', models.TextField(default='', max_length=128, verbose_name='header')),
                ('title', models.CharField(default='', max_length=128, verbose_name='title')),
                ('description', models.TextField(blank=True, max_length=512, verbose_name='description')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
                ('image', libs.stdimage.fields.StdImageField(aspects=('normal',), default='', min_dimensions=(700, 700), storage=libs.storages.media_storage.MediaStorage('webpage/images'), upload_to='', variations={'admin': {'size': (200, 200)}, 'normal': {'create_webp': True, 'size': (700, 700)}}, verbose_name='image')),
            ],
            options={
                'verbose_name': 'settings',
                'default_permissions': ('change',),
            },
        ),
    ]
