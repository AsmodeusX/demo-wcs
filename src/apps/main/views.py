from django.views.generic import DetailView
from seo.seo import Seo
from .models import MainPageConfig


class IndexView(DetailView):
    model = MainPageConfig
    context_object_name = 'page'
    template_name = 'main/index.html'

    def get_object(self, queryset=None):
        return self.model.get_solo()

    def get_context_data(self, **kwargs):
        seo = Seo()
        seo.set_data(self.object, defaults={
            'title': self.object.header,
            'og_title': self.object.header,
        })
        seo.save(self.request)

        context = super().get_context_data(**kwargs)
        context.update({
            'is_main_page': True,  # отменяет <noindex> шапки и подвала
        })
        return context
