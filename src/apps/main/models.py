from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel
from webpage.models import StdImagePage


class MainPageConfig(SingletonModel, StdImagePage):

    class Meta:
        default_permissions = ('change', )
        verbose_name = _('settings')

    def get_absolute_url(self):
        return resolve_url('main:index')

    def __str__(self):
        return ugettext('Home page')
