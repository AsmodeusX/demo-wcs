(function($) {

	var radius = 8;

	$.visibilityInspector.inspect('.jelly--lazy', {
		top: 20,
		bottom: 20,
		afterCheck: function($elem, opts, state) {
			if (!$elem.hasClass('loaded') && state) {
				$elem.addClass('loaded');
				var url_img = window.use_webp ? $elem.data('image-webp') : $elem.data('image');
				$elem.find('image').attr('href', url_img);
			}
		}
	});

	TweenMax.staggerFromTo('.blob', 4 ,{
		cycle: {
			attr:function(i) {
				var r = i*90;
				return {
					transform:'rotate('+r+') translate('+radius+',0.1) rotate('+(-r)+')'
				}
			}
		}
	},{
		cycle: {
			attr:function(i) {
				var r = i*90+360;
				return {
					transform:'rotate('+r+') translate('+radius+',0.1) rotate('+(-r)+')'
				}
			}
		},
		ease:Linear.easeNone,
		repeat:-1
	});

})(jQuery);