from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage
from users.models import CustomUser


class StdPage(models.Model):
    header = models.TextField(_('header'), max_length=128, default='', )
    title = models.CharField(_('title'), max_length=128, default='', )
    description = models.TextField(_('description'), blank=True, max_length=512)

    updated = models.DateTimeField(_('change date'), auto_now=True, blank=True,)

    class Meta:
        abstract = True


class StdImagePage(StdPage):
    image = StdImageField(_('image'),
                          default='',
                          storage=MediaStorage('webpage/images'),
                          min_dimensions=(700, 700),
                          admin_variation='admin',
                          crop_area=True,
                          aspects=('normal',),
                          variations=dict(
                              normal=dict(
                                  size=(700, 700),
                                  create_webp=True,
                              ),
                              admin=dict(
                                  size=(200, 200),
                              ),
                          ),
                          )

    class Meta:
        abstract = True


class TextPage(models.Model):
    author = models.ForeignKey(CustomUser, null=True, blank=True)
    share_block = models.BooleanField(_('share block'), default=True)
    text_info_block = models.BooleanField(_('date and author'), default=False)
    date = models.DateTimeField(_('publication date'), default=now, blank=True)

    class Meta:
        abstract = True
