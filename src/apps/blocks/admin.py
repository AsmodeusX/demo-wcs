from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import (
    AdvantagesBlock, Advantage, FAQBlock, Rule,
    AboutBlock, ServicesBlock, Service, Question
)
from suit.admin import SortableStackedInline
from project.admin.base import ModelAdminInlineMixin
from attachable_blocks.admin import AttachableBlockAdmin


# Advantages
class AdvantagesInline(ModelAdminInlineMixin, SortableStackedInline):
    model = Advantage
    min_num = 0
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-general'


@admin.register(AdvantagesBlock)
class AdvantagesBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
    )
    inlines = (AdvantagesInline, )


# Services
class ServicesInline(ModelAdminInlineMixin, SortableStackedInline):
    model = Service
    min_num = 0
    max_num = 3
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-general'

    prepopulated_fields = {
        'slug': ('title', ),
    }


@admin.register(ServicesBlock)
class ServicesBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
    )
    inlines = (ServicesInline, )


# About
class RulesInline(ModelAdminInlineMixin, SortableStackedInline):
    model = Rule
    min_num = 0
    max_num = 2
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-general'


@admin.register(AboutBlock)
class AboutBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'description',
            ),
        }),
        (_('Founder'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'founder_photo', 'founder_caption',
            ),
        }),
    )
    inlines = (RulesInline, )


# FAQ
class QuestionsInline(ModelAdminInlineMixin, SortableStackedInline):
    model = Question
    min_num = 0
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-general'

    prepopulated_fields = {
        'slug': (
            'question',
        )
    }


@admin.register(FAQBlock)
class FAQBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title',
            ),
        }),
    )
    inlines = (QuestionsInline, )

