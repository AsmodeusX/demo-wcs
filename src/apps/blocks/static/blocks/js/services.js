/** @namespace window.ajax_views.blocks.service */


(function($) {
    function service_popup(response) {
        $.popup({
            content: response.popup
        }).on('after_show', function () {
            var $popupWrap = $('.popup--service .popup__wrap');
            var maxHeightPopup = $.winHeight() * 0.66,
                curHeightPopup = $popupWrap.eq(0).innerHeight();
            prepareTextStyles($popupWrap);
            if (curHeightPopup > maxHeightPopup) {
                addScroll()
            }
        }).on('after_hide', function () {
            window.location.hash = "services";
        }).show();
    }

    var $block;

    $(document).ready(function() {
        $block = $('.block--services');
    });

    $(document).on('click', '.block--services .service', function(e) {
        var service_id = $(this).data('id');
        showServicePopup(service_id);
        return false;
    });

    function showServicePopup(service_id) {
        var data = [];

        data.push({
            name: 'service_id',
            value: service_id
        });

        return $.ajax({
            url: window.ajax_views.blocks.service,
            type: 'GET',
            data: data,
            success: function(response) {
                service_popup(response);
                window.location.hash = response.slug
            }
        });
    }
    function addScroll() {
        $(".popup--service").mCustomScrollbar({
            setHeight: $('.popup--service .popup__wrap').eq(0).innerHeight() - 20,
            advanced:{
                updateOnImageLoad: false,
                updateOnContentResize:false
            }
        });
    }

})(jQuery);
