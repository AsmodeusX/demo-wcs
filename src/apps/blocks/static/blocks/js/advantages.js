(function($) {

    $(window).on('load', function() {
        var $blocks = $('.block--advantages');
        $blocks.each(function () {
            var $this = $(this);
            setSwiper($this);
        });
    });

    function setSwiper($elem) {
        var w = $.winWidth();

        new Swiper($elem.find('.advantages').get(0), {
            spaceBetween: 20,
            threshold: 10,
            slidesPerGroup: w >= 1280 ? 4 : w >= 768 ? 2 : 1,
            width: 286,
            watchSlidesVisibility: true,
            watchOverflow: true,
            pagination: {
                el: $elem.find('.swiper-pagination').get(0),
                type: 'progressbar'
            },
            navigation: {
                nextEl: $elem.find('div.swiper-button-next').get(0),
                prevEl: $elem.find('div.swiper-button-prev').get(0)
            }
        });
    }

})(jQuery);