(function($) {

    $(window).ready(function() {
        var $blocks = $('section.block--about');
        $blocks.each(function () {
            var $block = $(this);
            setExpander($block);
        });
    });

    function setExpander($block) {
        var $description = $block.find('div.information__description').eq(0);

        $description.expander({
            speed: 800,
            after_expand: function(event, data) {
                data.button.text(gettext('Read Less'));
            },
            after_reduce: function(event, data) {
                data.button.text(gettext('Read More'));
            }
        });

    }

})(jQuery);