(function($) {
    var clActive = 'active';

    $(window).ready(function() {
        var $blocks = $('section.block--faq');

        $blocks.each(function () {
            var $block = $(this),
                $tabs = $block.find('.tab span'),
                $questions = $block.find('.question');

            resetTabs($tabs);

            activateTab($tabs.eq(0));

            $tabs.on('click', function (e) {
                e.preventDefault();

                var $tab = $(e.currentTarget);

                activateTab($tab);
            })
            var $questions_inner = $questions.find('.question__question');
            $questions_inner.on('click', function (e) {
                e.preventDefault();
                var $question = $(e.currentTarget),
                    $answer = $question.parent().find('.question__answer').eq(0);

                $question.closest('.question').toggleClass('active');
                changeStateAnswer($answer);

                if ($question.closest('.question').hasClass('active')) {
                    window.location.hash = $question.closest('.question').data('hash')
                } else {
                    window.location.hash = 'faq'
                }
            })
        });
    });

    function activateTab($tab) {
        $('ul.questions').removeClass(clActive);
        $('.tab').removeClass(clActive);
        $tab = $tab.closest('.tab');
        $tab.addClass(clActive);
        var tab = $tab.data('tab');
        $("ul.questions[data-tab='" + tab + "']").addClass(clActive);
    }

    function changeStateAnswer($answers) {
        $answers.slideToggle('slow');
    }

    function resetTabs() {
        var $questions = $('.question'),
            $answers = $questions.find('.question__answer');

        $answers.slideUp(0);
        $('.questions').removeClass(clActive);
    }

})(jQuery);