from django.shortcuts import loader
from libs.cache.cached import cached


@cached('block.id', time=5*60)
def about_block_render(context, block, **kwargs):

    return loader.render_to_string('blocks/about.html', {
        'block': block,
    }, request=context.get('request'))


# @cached('block.id', time=5*60)
def advantages_block_render(context, block, **kwargs):

    return loader.render_to_string('blocks/advantages.html', {
        'block': block,
    }, request=context.get('request'))


@cached('block.id', time=5*60)
def faq_block_render(context, block, **kwargs):

    return loader.render_to_string('blocks/faq.html', {
        'block': block,
    }, request=context.get('request'))


@cached('block.id', time=5*60)
def services_block_render(context, block, **kwargs):
    return loader.render_to_string('blocks/services.html', {
        'block': block,
    }, request=context.get('request'))
