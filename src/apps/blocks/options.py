ADVANTAGES_Y = -152
ABOUT_Y = -186


def fixed_position(pos_x):
    sprite_move_x = 18

    return -(pos_x + sprite_move_x)


ADVANTAGES_ICONS = (
    ('solutions', (fixed_position(0), ADVANTAGES_Y)),
    ('transparency', (fixed_position(34), ADVANTAGES_Y)),
    ('honesty', (fixed_position(68), ADVANTAGES_Y)),
    ('commissions', (fixed_position(102), ADVANTAGES_Y)),
    ('inspections', (fixed_position(136), ADVANTAGES_Y)),
    ('hassle', (fixed_position(170), ADVANTAGES_Y)),
    ('buying', (fixed_position(204), ADVANTAGES_Y)),
    ('pool', (fixed_position(238), ADVANTAGES_Y)),
    ('professional', (fixed_position(272), ADVANTAGES_Y)),
)
ABOUT_ICONS = (
    ('team', (fixed_position(0), ABOUT_Y)),
    ('network', (fixed_position(34), ABOUT_Y)),
)
