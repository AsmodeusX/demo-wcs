from django.template import loader
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import View
from ajax_views.decorators import ajax_view
from .models import Service


@ajax_view('blocks.service')
class ServicePopup(View):

    def get(self, request):
        service_id = request.GET.get('service_id')

        try:
            service = Service.objects.get(id=service_id)
        except ObjectDoesNotExist:
            return JsonResponse({}, status=400)

        if service:
            template = 'blocks/popups/service.html'

            return JsonResponse({
                'slug': service.slug,
                'popup': loader.render_to_string(template, {
                    'service': service
                })
            })

        return JsonResponse({}, status=400)
