from django.db import models
from attachable_blocks.models import AttachableBlock
from django.template.defaultfilters import linebreaksbr
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import CKEditorUploadField
from libs.sprite_image.fields import SpriteImageField
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage
# from libs.description import description
from libs.autoslug import AutoSlugField

from . import options


class AdvantagesBlock(AttachableBlock):
    BLOCK_VIEW = 'blocks.views.advantages_block_render'
    title = models.CharField(_('title'), max_length=128,
                             default='Advantages of working with WCS Property Solutions')

    class Meta:
        verbose_name = _('Advantages block')
        verbose_name_plural = _('Advantages blocks')


class Advantage(models.Model):
    block = models.ForeignKey(AdvantagesBlock, verbose_name=_('block'), related_name='advantages')
    title = models.CharField(_('title'), max_length=128)
    description = models.TextField(_('description'), )
    icon = SpriteImageField(_('icon'),
                            choices=options.ADVANTAGES_ICONS,
                            default=options.ADVANTAGES_ICONS[0][0],
                            sprite='img/sprite.svg',
                            size=(34, 34),
                            )

    sort_order = models.PositiveSmallIntegerField(_('sort order'), default=0)

    class Meta:
        verbose_name = _('Advantage')
        verbose_name_plural = _('Advantages')
        ordering = ('sort_order', )

    def __str__(self):
        return self.title


class ServicesBlock(AttachableBlock):
    BLOCK_VIEW = 'blocks.views.services_block_render'
    title = models.CharField(_('title'), max_length=128, default='What We Offer')

    class Meta:
        verbose_name = _('Services block')
        verbose_name_plural = _('Services blocks')


class Service(models.Model):
    block = models.ForeignKey(ServicesBlock, verbose_name=_('block'), related_name='services')
    preview = StdImageField(_('preview'),
                            storage=MediaStorage('blocks/services/previews'),
                            min_dimensions=(280, 320),
                            admin_variation='admin',
                            crop_area=True,
                            aspects=('normal',),
                            variations=dict(
                                normal=dict(
                                    size=(280, 320),
                                    create_webp=True,
                                ),
                                tablet=dict(
                                    size=(230, 260),
                                    create_webp=True,
                                ),
                                mobile=dict(
                                    size=(335, 220),
                                    create_webp=True,
                                ),
                                admin=dict(
                                    size=(335, 220),
                                ),
                            ),
                            )
    title = models.CharField(_('title'), max_length=128)
    question = models.CharField(_('question'), max_length=128)
    text = CKEditorUploadField(_('text'), blank=True)

    image = StdImageField(_('image'),
                          blank=True,
                          storage=MediaStorage('blocks/services/images'),
                          min_dimensions=(780, 230),
                          admin_variation='admin',
                          crop_area=True,
                          aspects=('normal',),
                          variations=dict(
                              normal=dict(
                                  size=(780, 230),
                                  create_webp=True,
                              ),
                              admin=dict(
                                  size=(390, 115),
                              ),
                          ),
                          )
    slug = AutoSlugField(_('slug'), populate_from='title', unique=False, default='')
    sort_order = models.PositiveSmallIntegerField(_('sort order'), default=0)

    class Meta:
        verbose_name = _('Service')
        verbose_name_plural = _('Services')
        ordering = ('sort_order', )

    def __str__(self):
        return self.title


class AboutBlock(AttachableBlock):
    BLOCK_VIEW = 'blocks.views.about_block_render'
    title = models.CharField(_('title'), max_length=128,
                             default='most important rule is honesty')
    description = models.TextField(_('description'),)
    founder_photo = StdImageField(_("founder photo"),
                                  blank=True,
                                  storage=MediaStorage('blocks/about/images'),
                                  min_dimensions=(350, 275),
                                  admin_variation='admin',
                                  crop_area=True,
                                  aspects=('normal',),
                                  variations=dict(
                                      normal=dict(
                                          size=(400, 400),
                                          stretch=True,
                                          create_webp=True,
                                          center=(-0.5, -0.5)
                                      ),
                                      admin=dict(
                                          size=(300, 300),
                                      ),
                                  ),
                                  )
    founder_caption = models.CharField(_('founder caption'), max_length=64, default='Maurice Sims, Founder')

    class Meta:
        verbose_name = _('About block')
        verbose_name_plural = _('About blocks')

    @property
    def short_description(self):
        parts = linebreaksbr(self.description).split('<br />')
        return parts[0]

    def is_long_description(self):
        parts = linebreaksbr(self.description).split('<br />')
        return len(parts) > 1

class Rule(models.Model):
    block = models.ForeignKey(AboutBlock, verbose_name=_('block'), related_name='rules')
    icon = SpriteImageField(_('icon'),
                            choices=options.ABOUT_ICONS,
                            default=options.ABOUT_ICONS[0][0],
                            sprite='img/sprite.svg',
                            size=(34, 34),
                            )
    title = models.CharField(_('title'), max_length=128)
    description = models.TextField(_('description'))

    sort_order = models.PositiveSmallIntegerField(_('sort order'), default=0)

    class Meta:
        verbose_name = _('Rule')
        verbose_name_plural = _('Rules')
        ordering = ('sort_order', )

    def __str__(self):
        return self.title

TYPE_QUESTIONS_BUYERS = 0
TYPE_QUESTIONS_SELLERS = 1

TYPES_QUESTION = (
    (TYPE_QUESTIONS_BUYERS, _('For Buyers')),
    (TYPE_QUESTIONS_SELLERS, _('For Sellers')),
)

class FAQBlock(AttachableBlock):
    BLOCK_VIEW = 'blocks.views.faq_block_render'
    title = models.CharField(_('title'), max_length=128,
                             default='FAQ')

    class Meta:
        verbose_name = _('FAQ block')
        verbose_name_plural = _('FAQ blocks')

    def questions_buyers(self):
        return self.questions.filter(type_question=TYPE_QUESTIONS_BUYERS)

    def questions_sellers(self):
        return self.questions.filter(type_question=TYPE_QUESTIONS_SELLERS)


class Question(models.Model):
    block = models.ForeignKey(FAQBlock, verbose_name=_('block'), related_name='questions')

    question = models.CharField(_('question'), max_length=128)
    answer = CKEditorUploadField(_('answer'), )
    type_question = models.PositiveSmallIntegerField(_('type question'), choices=TYPES_QUESTION, default=0)
    sort_order = models.PositiveSmallIntegerField(_('sort order'), default=0)
    slug = AutoSlugField(_('slug'), populate_from='question', unique=False, default='')

    class Meta:
        verbose_name = _('Question')
        verbose_name_plural = _('Questions')
        ordering = ('sort_order', )

    def __str__(self):
        return self.question

