from django.conf.urls import url
from . import views


app_name = 'contacts'
urlpatterns = [
    url(r'^thank-you/$', views.SuccessView.as_view(), name='success'),
]
