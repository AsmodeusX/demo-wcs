from .models import ContactsConfig


def contacts(request):
    contacts = ContactsConfig.get_solo()

    return {
        'contacts': contacts,
    }