from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.validators import EmailValidator
from libs.form_helper.forms import FormHelperMixin
from libs.widgets import PhoneWidget
from .models import Message
from libs.phone import translate_phonewords, is_e164_format
import re


class ContactForm(FormHelperMixin, forms.ModelForm):
    default_field_template = 'form_helper/field.html'

    class Meta:
        model = Message
        fields = '__all__'
        exclude = ('property', 'is_spam')
        labels = {
            'name': 'Name*',
            'phone': 'Phone*',
            'email': 'Email*',
            'message': 'Message',
        }
        widgets = {
            'name': forms.TextInput(attrs={
                'placeholder': _('John'),
            }),
            'phone': PhoneWidget(attrs={
                'placeholder': _('123-456-78-90'),
            }),
            'email': forms.EmailInput(attrs={
                'placeholder': _('john@gmail.com'),
            }),
            'message': forms.Textarea(attrs={
                'placeholder': _('Write your message'),
                'rows': 5,
            })
        }
        error_messages = {
            'name': {
                'required': _('Please enter your name'),
                'max_length': _('Name should not be longer than %(limit_value)d characters'),
                'min_length': _("The name is very short"),
                'incorrect': _('Your name contains illegal characters'),
            },
            'phone': {
                'required': _('Please enter your phone so we can contact you'),
                'max_length': _('Phone should not be longer than %(limit_value)d characters'),
                'min_length': _("The phone is very short"),
                'incorrect': _('Your phone contains illegal characters'),
                'invalid': _('Please enter correct phone'),

            },
            'email': {
                'required': _('Please enter your e-mail so we can contact you'),
                'max_length': _('Email should not be longer than %(limit_value)d characters'),
                'min_length': _("The email is very short"),
                'incorrect': _('Your email contains illegal characters'),
                'invalid': _('Please enter correct email'),

            },
            'message': {
                'required': _('Please enter your message'),
                'max_length': _('Message should not be longer than %(limit_value)d characters'),
                'min_length': _("The message is very short"),
                'incorrect': _('Your message contains illegal characters'),

            },
        }

    # Бессмысленно, т.к. при required
    # будет отрабатывать встроенная
    # проверка браузера на заполнение,
    # а нужна кастомная

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #
    #     for field in self.fields.values():
    #         if field.label:
    #             field.label += '*' if field.required else ''

    def clean_name(self):
        if 'name' in self.cleaned_data:
            name = self.cleaned_data.get('name')

            if not name:
                self.add_field_error('name', 'required')
            else:
                r = re.findall(r'[а-яА-я]', name)

                if len(r) > 0:
                    self.add_field_error('name', 'incorrect')
                else:
                    r = re.findall(r'[a-zA-Z]', name)

                    if len(r) < 3:
                        self.add_field_error('name', 'min_length')

            return name

    def clean_phone(self):
        if 'phone' in self.cleaned_data:
            phone = self.cleaned_data.get('phone')

            if not phone:
                self.add_field_error('phone', 'required')
            else:
                r = re.findall(r'[а-яА-я]', phone)

                if len(r) > 0:
                    self.add_field_error('phone', 'incorrect')
                else:
                    phone = translate_phonewords(phone)
                    is_phone = False

                    if is_e164_format(phone):
                        is_phone = True

                    if is_phone:
                        self.cleaned_data['phone'] = phone
                    else:
                        self.add_field_error('phone', 'invalid')

            return phone

    def clean_email(self):
        """ Требуем указать email """
        if 'email' in self.cleaned_data:
            email = self.cleaned_data.get('email')

            if not email:
                self.add_field_error('email', 'required')
            else:
                r = re.findall(r'[а-яА-я]', email)

                if len(r) > 0:
                    self.add_field_error('email', 'incorrect')
                else:
                    is_email = False

                    try:
                        EmailValidator()(email)
                        is_email = True
                    except forms.ValidationError:
                        pass

                    if is_email:
                        self.cleaned_data['email'] = email
                    else:
                        self.add_field_error('email', 'invalid')

            return email

    def clean_message(self):
        if 'message' in self.cleaned_data:
            message = self.cleaned_data.get('message')

            if not message:
                pass
                # self.add_field_error('message', 'required')
            else:
                r = re.findall(r'[а-яА-я]', message)

                if len(r) > 0:
                    self.add_field_error('message', 'incorrect')
                else:
                    r = re.findall(r'[a-zA-Z]', message)

                    if len(r) < 3:
                        self.add_field_error('message', 'min_length')

            return message
