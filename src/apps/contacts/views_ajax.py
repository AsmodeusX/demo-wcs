from django.shortcuts import resolve_url
from django.http import JsonResponse
from django.utils.html import escape
from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from ajax_views.decorators import ajax_view
from libs.email import send_template
from libs.check_recaptcha import check_recaptcha
from .models import NotificationReceiver, StopWord
from properties.models import Property
from .forms import ContactForm


@ajax_view('contacts.message')
class Message(View):
    def post(self, request, *args, **kwargs):
        captcha_result = check_recaptcha(request)
        property_slug = request.POST.get('property')
        featured_property = None

        if property_slug is not None:
            try:
                featured_property = Property.objects.get(slug=property_slug)
            except ObjectDoesNotExist:
                pass
        form = ContactForm(request.POST)

        if form.is_valid() and captcha_result:
            message = form.save(commit=False)
            referer = request.POST.get('referer')
            message.referer = escape(referer)

            if featured_property:
                message.property = featured_property

            is_spam = False
            stop_words = StopWord.objects.all().values_list('text', flat=True)

            for i in stop_words:
                stop_word = i.strip().lower()

                if stop_word in message.message.lower() or stop_word in message.name.lower():
                    is_spam = True
                    break

            if is_spam:
                message.is_spam = is_spam

            message.save()

            if not is_spam:
                receivers = NotificationReceiver.objects.all().values_list('email', flat=True)

                send_template(request, receivers,
                              subject=_('Message from {domain}'),
                              template='contacts/mails/message.html',
                              context={
                                  'message': message,
                              }
                              )

            return JsonResponse({
                'url': resolve_url('contacts:success')
            }, status=200)

        return JsonResponse({
            'errors': form.error_dict_full
        }, status=400)
