(function($) {

    $(document).ready(function () {
        $('#result_list tbody tr').on('click', function (e) {
            if ($(e.target).attr('type') !== 'checkbox') {
                window.location.href = $(this).find('a').attr('href');
            }
        });
    });

})(jQuery);
