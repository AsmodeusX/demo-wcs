/** @namespace window.ajax_views.contacts.message */


(function ($) {
    var recaptchaId;

    window.fnSend = function(token) {
        if (window.form && window.form.hasClass('sending')) return false;

        var data = window.form.serializeArray();
        // добавление адреса страницы, откуда отправлена форма
        data.push({
            name: 'referer',
            value: window.location.href
        });
        var property = get("property");

        if (property !== undefined)
            data.push({
                name: 'property',
                value: property
            });

        data.push({
            name: 'g_recaptcha_response',
            value: token
        });

        $.ajax({
            url: window.ajax_views.contacts.message,
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                window.form.find('.invalid').removeClass('invalid');
                window.form.find('.error-message').remove();
                window.form.addClass('sending');
                // $form.find('.invalid').removeClass('invalid');
                $.preloader();
            },
            success: function (response) {
                if (response.url) window.location.href = response.url;
            },
            error: $.parseError(function (response) {
                $.popup().hide();

                if (response) {
                    // ошибки формы
                    renderErrors(response, window.form);

                } else {
                    alert(window.DEFAULT_AJAX_ERROR);
                }
            }),
            complete: function () {
                window.grecaptcha.reset(recaptchaId);
                window.form.removeClass('sending');
            }
        });
    };

    function renderErrors(response) {
        $.each(response.errors, function () {
            var error = this,
                $field = window.form.find('div.' + error.fullname);
            $field.addClass('invalid');
            var $errorMessage = $('<span />').addClass('error-message'),
                $errorMessageText = $('<span />').html(error.errors[0]);
            $field.find('div.control').append(
                $errorMessage.append(
                    $errorMessageText
                )
            );
        })
    }

    function get(name){
        if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
            return decodeURIComponent(name[1]);
    }
})(jQuery);
