from django.db import models
from django.utils.timezone import now
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel
from attachable_blocks.models import AttachableBlock
from webpage.models import StdPage
from properties.models import Property


class ContactsConfig(SingletonModel):
    phone = models.CharField(_('phone'), max_length=255, default='(303) 218-3497')
    email = models.EmailField(_('email'), max_length=255, default='mauricesims@wcspropertysolutions.com')
    address = models.CharField(_('address'), max_length=255, default='P.O. Box 201912')
    city = models.CharField(_('city'), max_length=255, default='Denver')
    region = models.CharField(_('region'), max_length=64, default='CO')
    zip = models.CharField(_('zip'), max_length=32, default='80220')

    class Meta:
        default_permissions = ('change', )
        verbose_name = _('Settings')

    def __str__(self):
        return ugettext('Settings')

    def full_address(self):
        return "{address} {city}, {region} {zip}".format(
            address=self.address,
            city=self.city,
            region=self.region,
            zip=self.zip,
        )


class NotificationReceiver(models.Model):
    config = models.ForeignKey(ContactsConfig, related_name='receivers')
    email = models.EmailField(_('e-mail'))

    class Meta:
        verbose_name = _('notification receiver')
        verbose_name_plural = _('notification receivers')

    def __str__(self):
        return self.email


class Area(models.Model):
    config = models.ForeignKey(ContactsConfig, related_name='areas')
    city = models.CharField(_('city'), max_length=255)
    region = models.CharField(_('region'), max_length=64, blank=True)

    sort_order = models.PositiveSmallIntegerField(_('sort order'), default=0)

    class Meta:
        verbose_name = _('Area')
        verbose_name_plural = _('Areas')
        ordering = ('sort_order', )

    def __str__(self):
        return "{city}, {region}".format(
            city=self.city,
            region=self.region,
        )


class SuccessConfig(StdPage, SingletonModel):

    class Meta:
        default_permissions = ('change', )
        verbose_name = _('success page')

    def get_absolute_url(self):
        return resolve_url('contacts:success')

    def __str__(self):
        return self.header


class StopWord(models.Model):
    text = models.CharField(_('text'), max_length=255)

    class Meta:
        verbose_name = _('stop word')
        verbose_name_plural = _('stop words')

    def __str__(self):
        return self.text


class Message(models.Model):
    name = models.CharField(_('name'), max_length=128, blank=True)
    phone = models.CharField(_('phone'), max_length=32, blank=True)
    email = models.EmailField(_('e-mail'), blank=True)
    message = models.TextField(_('message'), max_length=2048, blank=True)
    property = models.ForeignKey(Property, verbose_name=_('property'), null=True, blank=True)
    is_spam = models.BooleanField(_('is spam'), default=False)
    date = models.DateTimeField(_('date sent'), default=now, editable=False)
    referer = models.TextField(_('from page'), blank=True, editable=False)

    class Meta:
        default_permissions = ('delete', )
        verbose_name = _('message')
        verbose_name_plural = _('messages')
        ordering = ('-date',)

    def __str__(self):
        return self.name


class ContactBlock(AttachableBlock):
    BLOCK_VIEW = 'contacts.views.contact_block_render'

    title = models.CharField(_('title'), max_length=128, default='')
    description = models.TextField(_('description'), blank=True)

    class Meta:
        default_permissions = ()
        verbose_name = _('Contact block')
        verbose_name_plural = _('Contact blocks')
