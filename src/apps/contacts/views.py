from django.template import loader
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView
from seo.seo import Seo
from .models import SuccessConfig
from libs.description import description
from libs.cache.cached import cached
from menu.utils import activate_menu_nolink
from .forms import ContactForm


class SuccessView(TemplateView):
    template_name = 'contacts/success.html'

    def get(self, request, *args, **kwargs):
        config = SuccessConfig.get_solo()
        prefixes = ['mobile', 'header', 'footer']

        for prefix in prefixes:
            activate_menu_nolink(self.request, 'contacts_%s' % prefix)

        seo = Seo()
        seo.set({
            'title': config.title or _('Thank You!'),
            'og_title': config.title or _('Thank You!'),
            'description': description(config.description, 50, 160),
            'og_description': config.description,
        })
        seo.save(self.request)

        return self.render_to_response({
            # 'title_h1': config.header,
            'page': config,
            'is_success_page': True,
        })


# @cached('block.id')
def contact_block_render(context, block, **kwargs):
    form = ContactForm()

    return loader.render_to_string('contacts/block.html', {
        'form': form,
        'block': block,
    }, request=context.get('request'))
