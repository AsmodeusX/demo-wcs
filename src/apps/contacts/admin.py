from django.conf import settings
from django.contrib import admin
from django.utils import dateformat
from django.utils.timezone import localtime
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from project.admin import ModelAdminMixin, ModelAdminInlineMixin
from attachable_blocks.admin import AttachableBlockAdmin, AttachedBlocksStackedInline
from seo.admin import SeoModelAdminMixin
from libs.description import description
from suit.admin import SortableTabularInline
from .models import (
    ContactsConfig, SuccessConfig, StopWord, Message,
    NotificationReceiver, ContactBlock, Area
)
from webpage.admin import SEOStdPageAdmin


class ContactsConfigBlocksInline(AttachedBlocksStackedInline):
    suit_classes = 'suit-tab suit-tab-blocks'


class NotificationReceiverAdmin(ModelAdminInlineMixin, admin.TabularInline):
    model = NotificationReceiver
    extra = 0
    suit_classes = 'suit-tab suit-tab-notify'


class AreasAdmin(ModelAdminInlineMixin, SortableTabularInline):
    model = Area
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-general'


@admin.register(ContactsConfig)
class ContactsConfigAdmin(SeoModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (_('Contacts'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'phone', 'email',
            ),
        }),
        (_('Address'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'address', 'city', 'region', 'zip',
            ),
        }),
    )
    inlines = (AreasAdmin, NotificationReceiverAdmin, ContactsConfigBlocksInline)
    suit_form_tabs = (
        ('general', _('General')),
        ('notify', _('Notifications')),
        ('blocks', _('Blocks')),
    )


class SpamFilter(admin.SimpleListFilter):
    title = _('Is Spam')
    parameter_name = 'is_spam'

    def value(self):
        value = super().value()
        if not value:
            value = '0'
        return value

    def lookups(self, request, model_admin):
        return (
            ('2', _('All')),
            ('1', _('Yes')),
            ('0', _('No')),
        )

    def queryset(self, request, queryset):
        if self.value() == '0':
            return queryset.filter(is_spam=False)
        if self.value() == '1':
            return queryset.filter(is_spam=True)
        return queryset


@admin.register(Message)
class MessageAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name', 'phone', 'email', 'property',
            ),
        }),
        (_('Text'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'message',
            ),
        }),
        (_('Info'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'is_spam', 'date_fmt', 'referer',
            ),
        }),
    )
    readonly_fields = ('name', 'property', 'phone', 'email', 'message', 'date_fmt', 'referer', 'is_spam')
    list_display = ('name', 'property', 'message_fmt', 'date_fmt', 'is_spam')
    list_filter = (SpamFilter,)
    suit_form_tabs = (
        ('general', _('General')),
    )

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def message_fmt(self, obj):
        return description(obj.message, 60, 80)
    message_fmt.short_description = _('Message')
    message_fmt.admin_order_field = 'message'

    def date_fmt(self, obj):
        return dateformat.format(localtime(obj.date), settings.DATETIME_FORMAT)
    date_fmt.short_description = _('Date')
    date_fmt.admin_order_field = 'date'

    class Media:
        css = {
            'all': (
                'contacts/admin/css/contacts.css',
            )
        }
        js = (
            'js/jquery.fakelink.js',
            'contacts/admin/js/contacts.js',
        )


@admin.register(SuccessConfig)
class SuccessConfigAdmin(SEOStdPageAdmin, SingletonModelAdmin):
    fieldsets = SEOStdPageAdmin.fieldsets
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(StopWord)
class StopWordAdmin(ModelAdminMixin, admin.ModelAdmin):
    """ Адрес """
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text',
            ),
        }),
    )
    list_display = ('text', )
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(ContactBlock)
class ContactBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'description',
            ),
        }),
    )
