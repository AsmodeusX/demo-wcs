from django.conf.urls import url
from libs.autoslug import ALIAS_REGEXP
from . import views


app_name = 'properties'
urlpatterns = [
    url(r'^(?P<slug>{0})/$'.format(ALIAS_REGEXP), views.PropertyView.as_view(), name='property'),
]
