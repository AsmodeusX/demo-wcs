from django.views.generic import DetailView
from django.shortcuts import loader
from seo.seo import Seo
from .models import Property
from breadcrumbs.utils import breadcrumb_main
from libs.cache.cached import cached


class PropertyView(DetailView):
    model = Property
    context_object_name = 'page'
    template_name = 'properties/property.html'

    def get_context_data(self, **kwargs):
        seo = Seo()
        seo.set_data(self.object, defaults={
            'title': self.object.full_address(),
            'og_title': self.object.full_address(),
            'og_image': self.object.preview,
        })
        seo.save(self.request)

        breadcrumb_main(self.request)
        self.request.breadcrumbs.add(self.object.full_address(), self.object.get_absolute_url())

        context = super().get_context_data(**kwargs)

        return context


@cached('block.id')
def properties_block_render(context, block, **kwargs):
    properties = Property.objects.filter(visible=True)
    return loader.render_to_string('properties/block.html', {
        'title': block.title,
        'description': block.description,
        'properties': properties,
        'block': block,
    }, request=context.get('request'))
