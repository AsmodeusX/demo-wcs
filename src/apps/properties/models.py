from django.db import models
from django.utils.timezone import now
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _, ugettext
from ckeditor.fields import CKEditorUploadField
from libs.autoslug import AutoSlugField
from gallery.fields import GalleryField
from gallery.models import GalleryImageItem, GalleryBase
from libs.valute_field.fields import ValuteField
from solo.models import SingletonModel
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage
from attachable_blocks.models import AttachableBlock

STATUS_AVAILABLE = 1
STATUS_NOT_AVAILABLE = 2
STATUS_SOLD = 3


class PropertiesConfig(SingletonModel):
    featured_properties_title = models.CharField(_('featured properties title'),
                                                 max_length=128,
                                                 default='Featured property')
    featured_properties_description = models.TextField(_('featured properties description'),
                                                       blank=True,
                                                       default='Browse your dream house')
    other_featured_properties_title = models.CharField(_('other featured properties title'),
                                                       max_length=128,
                                                       default='Featured property')
    other_featured_properties_description = models.TextField(_('other featured properties title'),
                                                             blank=True,
                                                             default='Browse your dream house')

    class Meta:
        default_permissions = ('change',)
        verbose_name = _('Settings')

    def __str__(self):
        return ugettext('Properties Config')


class CustomGalleryImageItem(GalleryImageItem):
    STORAGE_LOCATION = 'properties/gallery'
    MIN_DIMENSIONS = (300, 200)
    ADMIN_VARIATION = 'admin'
    SHOW_VARIATION = 'normal'
    ASPECTS = 'normal'
    VARIATIONS = dict(
        normal=dict(
            stretch=True,
            size=(960, 600),
            create_webp=True,
        ),
        micro=dict(
            stretch=True,
            size=(190, 130),
            create_webp=True,
        ),
        admin=dict(
            stretch=True,
            size=(190, 130),
        ),
    )


class CustomGallery(GalleryBase):
    IMAGE_MODEL = CustomGalleryImageItem


class ListingType(models.Model):
    title = models.CharField(_('title'), max_length=128)

    class Meta:
        verbose_name = _('Listing type')
        verbose_name_plural = _('Listing types')
        ordering = ('title', )

    def __str__(self):
        return self.title


class PropertyType(models.Model):
    title = models.CharField(_('title'), max_length=128)

    class Meta:
        verbose_name = _('Property type')
        verbose_name_plural = _('Property types')
        ordering = ('title', )

    def __str__(self):
        return self.title


class Feature(models.Model):
    title = models.CharField(_('title'), max_length=128)

    class Meta:
        verbose_name = _('Feature')
        verbose_name_plural = _('Features')
        ordering = ('title',)

    def __str__(self):
        return self.title


class Property(models.Model):
    PRICE_SALE = 1
    PRICE_RENT = 2

    TYPES_PRICE = (
        (PRICE_SALE, _('Sale')),
        (PRICE_RENT, _('Rent')),
    )

    STATUSES = (
        (STATUS_AVAILABLE, _('Available')),
        (STATUS_NOT_AVAILABLE, _('Not Available')),
        (STATUS_SOLD, _('Sold')),
    )
    preview = StdImageField(_('preview'),
                            storage=MediaStorage('properties/previews'),
                            min_dimensions=(380, 215),
                            admin_variation='admin',
                            crop_area=True,
                            aspects=('normal',),
                            variations=dict(
                                normal=dict(
                                    size=(380, 215),
                                    create_webp=True,
                                ),
                                mobile=dict(
                                    size=(335, 215),
                                    create_webp=True,
                                ),
                                admin=dict(
                                    size=(335, 215),
                                    create_webp=True,
                                ),
                            ),
                            )
    listing_type = models.ManyToManyField(ListingType, verbose_name=_('listing type'))
    property_type = models.ManyToManyField(PropertyType, verbose_name=_('property type'))
    features = models.ManyToManyField(Feature, verbose_name=_('features'), related_name='properties', blank=True)

    beds_count = models.PositiveSmallIntegerField(_('beds count'), blank=True, null=True)
    baths_count = models.PositiveSmallIntegerField(_('baths count'), blank=True, null=True)
    property_id = models.CharField(_('property id'), blank=True, max_length=64)
    rooms = models.PositiveSmallIntegerField(_('rooms'), default=1, blank=True, null=True)
    square_foot = models.FloatField(_('square foot'), help_text=_('SqFt'), blank=True, null=True)
    lot_sf = models.FloatField(_('lot sf'), help_text=_('SqFt'), blank=True, null=True)
    land_area = models.FloatField(_('land area'), help_text=_('SqFt'), blank=True, null=True)
    garages = models.PositiveSmallIntegerField(_('garages'), default=0, blank=True, null=True)
    garage_size = models.FloatField(_('garage size'), help_text=_('SqFt'), blank=True, null=True)

    price = ValuteField(_('price'))
    type_price = models.PositiveSmallIntegerField(_('type price'), choices=TYPES_PRICE, default=PRICE_RENT)

    status = models.PositiveSmallIntegerField(_('status'), choices=STATUSES, default=STATUS_AVAILABLE)
    gallery = GalleryField(CustomGallery, verbose_name=_('gallery'), blank=True, null=True)

    address = models.CharField(_('address'), max_length=255)
    city = models.CharField(_('city'), max_length=255)
    region = models.CharField(_('region'), max_length=64)
    zip = models.CharField(_('zip'), max_length=32, blank=True)
    description = CKEditorUploadField(_('description'), blank=True)

    sort_order = models.PositiveSmallIntegerField(_('sort order'), default=0)

    slug = AutoSlugField(_('slug'), populate_from='address', unique=True)
    visible = models.BooleanField(_('visible'), default=True)
    date = models.DateTimeField(_('publication date'), default=now)
    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('Property')
        verbose_name_plural = _('Properties')
        ordering = ('sort_order', )

    def full_address(self):
        return '{address} | {city}, {region}'.format(
            address=self.address,
            city=self.city,
            region=self.region
        )

    def google_link(self):
        return 'https://maps.google.com/?q={address} {city}, {region} {zip}'.format(
            address=self.address,
            city=self.city,
            region=self.region,
            zip=self.zip
        )

    def __str__(self):
        return self.full_address()

    def get_absolute_url(self):
        return resolve_url('properties:property', slug=self.slug)

    def str_listing_type(self):
        if self.listing_type:
            return ' / '.join(self.listing_type.values_list('title', flat=True))

        return ''

    def str_property_type(self):
        if self.listing_type:
            return ' / '.join(self.property_type.values_list('title', flat=True))

        return ''

    @staticmethod
    def format_sqft(val):
        if val:
            format_val = ('%s' % '{0:g}'.format(val))
            parts_format_val = format_val.split('.')
            format_val_whole = parts_format_val[0]

            new_val = ','.join('{:,}'.format(int(format_val_whole)).split(','))

            if len(parts_format_val) > 1:
                format_val_remainder = parts_format_val[1]
                new_val = "%s.%s" % (new_val, format_val_remainder)
            return '%s SqFt' % new_val
        return None

    def str_square_foot(self):
        return self.format_sqft(self.square_foot)

    def str_lot_sf(self):
        return self.format_sqft(self.lot_sf)

    def str_land_area(self):
        return self.format_sqft(self.land_area)

    def str_garage_size(self):
        return self.format_sqft(self.garage_size)

    def str_price(self):
        if self.type_price == self.PRICE_RENT:
            return "%s / Per Month" % self.price
        return self.price

    def buy_url(self):
        return '%s?property=%s#contacts' % (resolve_url('main:index'), self.slug)


class PropertiesBlock(AttachableBlock):
    BLOCK_VIEW = 'properties.views.properties_block_render'
    title = models.CharField(_('title'), max_length=128,
                             default='Featured property')
    description = models.TextField(_('description'),
                                   default='Browse your dream house', blank=True)

    class Meta:
        verbose_name = _('Featured Properties block')
        verbose_name_plural = _('Featured Properties blocks')
