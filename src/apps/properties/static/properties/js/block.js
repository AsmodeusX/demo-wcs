(function($) {
    /** @namespace gettext */

    $(window).on('load', function () {
        var $blocks = $('.block--featured-properties');
        var w = $.winWidth();

        $blocks.each(function () {
            var $block = $(this);

            new Swiper($block.find('.properties').get(0), {
                spaceBetween: 20,
                watchSlidesOverflow: true,

                slidesPerView: w >= 1280 ? 3 : w >= 768 ? 2 : 1,
                // slidesPerGroup: w >= 1280 ? 3 : w >= 768 ? 2 : 1,
                navigation: {
                    nextEl: $block.find('div.swiper-button-next').get(0),
                    prevEl: $block.find('div.swiper-button-prev').get(0)
                },
                pagination: {
                    el: $block.find(".swiper-pagination").get(0),
                    type: "progressbar"
                }

            });
        });
    });
})(jQuery);
