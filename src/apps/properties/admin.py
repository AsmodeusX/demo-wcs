from django import forms
from django.contrib import admin
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from suit.admin import SortableModelAdmin
from project.admin import ModelAdminMixin
from seo.admin import SeoModelAdminMixin
from attachable_blocks.admin import AttachedBlocksStackedInline, AttachableBlockAdmin
from libs.autocomplete.widgets import AutocompleteMultipleWidget
from .models import (PropertyType, ListingType, Property,
                     Feature, PropertiesConfig, PropertiesBlock,
                     STATUS_AVAILABLE, STATUS_NOT_AVAILABLE, STATUS_SOLD)


class PropertyBlocksInline(AttachedBlocksStackedInline):
    suit_classes = 'suit-tab suit-tab-blocks'


@admin.register(PropertiesConfig)
class PropertiesConfigAdmin(SeoModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'featured_properties_title',
                'featured_properties_description',
                'other_featured_properties_title',
                'other_featured_properties_description',
            ),
        }),
    )
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(PropertyType)
class PropertyTypeAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('title', ),
        }),
    )
    list_display = ('title',)


@admin.register(ListingType)
class ListingTypeAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('title', ),
        }),
    )
    list_display = ('title',)


@admin.register(Feature)
class FeatureAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('title', ),
        }),
    )
    list_display = ('title',)


class PropertyForm(forms.ModelForm):
    class Meta:
        model = Property
        fields = '__all__'
        widgets = {
            'property_type': AutocompleteMultipleWidget(
                attrs={
                    'style': 'width:50%',
                }
            ),
            'listing_type': AutocompleteMultipleWidget(
                attrs={
                    'style': 'width:50%',
                }
            ),
            'features': AutocompleteMultipleWidget(
                attrs={
                    'style': 'width:50%',
                }
            ),
        }


@admin.register(Property)
class PropertyAdmin(SeoModelAdminMixin, SortableModelAdmin):
    fieldsets = (
        (_('Address'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'address', 'city', 'region', 'zip',
            ),
        }),
        (_('Specifications'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'beds_count', 'baths_count', 'square_foot',
                'lot_sf', 'property_id', 'rooms',
                'land_area', 'garages', 'garage_size',
            ),
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'listing_type', 'property_type', 'features',
                'preview', 'type_price', 'price', 'status',
                'description', 'gallery', 'slug', 'visible',
            ),
        }),
    )
    sortable = 'sort_order'
    form = PropertyForm
    inlines = (PropertyBlocksInline, )
    list_display = (
        'full_address', 'beds_count', 'baths_count',
        'property_id', 'rooms', 'format_str_square_foot',
        'format_str_lot_sf', 'format_str_listing_type', 'format_str_property_type',
        'format_str_land_area', 'garages', 'format_str_garage_size',
        'type_price', 'format_str_price', 'status', 'format_google_link', 'visible',
    )
    list_filter = ('status', )
    actions = ('make_available', 'make_not_available', 'make_sold', )
    suit_form_tabs = (
        ('general', _('General')),
        ('blocks', _('Blocks')),
    )
    prepopulated_fields = {
        'slug': (
            'address',
        )
    }

    def format_google_link(self, obj):
        return format_html("<a href='{url}' target='_blank' rel='nofollow noopener'>{txt}</a>", url=obj.google_link(), txt=_('Open link'))
    format_google_link.short_description = "Google URL"

    def format_str_square_foot(self, obj):
        return obj.str_square_foot()
    format_str_square_foot.short_description = "Square Foot"

    def format_str_lot_sf(self, obj):
        return obj.str_lot_sf()
    format_str_lot_sf.short_description = "Lot SF"


    def format_str_listing_type(self, obj):
        return obj.str_listing_type()
    format_str_listing_type.short_description = "Listing Type"


    def format_str_property_type(self, obj):
        return obj.str_property_type()
    format_str_property_type.short_description = "Property Type"


    def format_str_land_area(self, obj):
        return obj.str_land_area()
    format_str_land_area.short_description = "Land Area"


    def format_str_garage_size(self, obj):
        return obj.str_garage_size()
    format_str_garage_size.short_description = "Garage Size"


    def format_str_price(self, obj):
        return obj.str_price()
    format_str_price.short_description = "Price"


    def make_available(self, request, queryset):
        queryset.update(status=STATUS_AVAILABLE)
    make_available.short_description = _('Make available')

    def make_not_available(self, request, queryset):
        queryset.update(status=STATUS_NOT_AVAILABLE)
    make_not_available.short_description = _('Make not available')

    def make_sold(self, request, queryset):
        queryset.update(status=STATUS_SOLD)
    make_sold.short_description = _('Make sold')




@admin.register(PropertiesBlock)
class FAQBlockAdmin(AttachableBlockAdmin):
    fieldsets = AttachableBlockAdmin.fieldsets + (
        (_('Customization'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'description',
            ),
        }),
    )
