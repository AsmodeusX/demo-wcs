from django.template import loader
from libs import jinja2
from ..models import Property, PropertiesConfig

BLOCK_TEMPLATE = 'properties/block.html'


@jinja2.extension
class HeaderExtension(jinja2.Extension):
    tags = {'featured_properties', 'other_featured_properties', }
    takes_context = True
    config = PropertiesConfig.get_solo()
    properties = Property.objects.filter(visible=True)

    def _featured_properties(self, context, template=None):

        return loader.render_to_string(template or BLOCK_TEMPLATE, {
            'properties': self.properties,
            'title': self.config.featured_properties_title or None,
            'description': self.config.featured_properties_description or None,
        }, request=context.get('request'))

    def _other_featured_properties(self, context, property_id, template=None):
        properties = self.properties.exclude(id=property_id)

        return loader.render_to_string(template or BLOCK_TEMPLATE, {
            'properties': properties,
            'title': self.config.other_featured_properties_title or None,
            'description': self.config.other_featured_properties_description or None,
        }, request=context.get('request'))
