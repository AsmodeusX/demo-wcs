from django.contrib.sitemaps import GenericSitemap
from main.models import MainPageConfig
from properties.models import Property

main_page = {
    'queryset': MainPageConfig.objects.all(),
    'date_field': 'updated',
}

property_page = {
    'queryset': Property.objects.filter(visible=True),
    'date_field': 'updated',
}


site_sitemaps = {
    'property': GenericSitemap(property_page, changefreq='weekly', priority=0.5),
    'main': GenericSitemap(main_page, changefreq='daily', priority=1),
}
