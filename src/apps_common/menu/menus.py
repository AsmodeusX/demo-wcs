from django.utils.translation import ugettext_lazy as _
from .base import Menu, MenuItem


def menu_mobile(request):
    menu = Menu()
    prefix = 'mobile'

    menu.append(
        MenuItem(
            title=_('Services'),
            url='/#services',
        ),
        MenuItem(
            title=_('Featured Property'),
            url='/#featured-properties',
        ),
        MenuItem(
            title=_('About Us'),
            url='/#about',
        ),
        MenuItem(
            title=_('FAQ'),
            url='/#faq',
        ),
        MenuItem(
            title=_('Contact Us'),
            url='/#contacts',
            item_id='contacts_%s' % prefix
        ),
    )

    return menu


def menu_header(request):

    menu = Menu()
    prefix = 'header'

    menu.append(
        MenuItem(
            title=_('Services'),
            url='/#services',
        ),
        MenuItem(
            title=_('Featured Property'),
            url='/#featured-properties',
        ),
        MenuItem(
            title=_('About Us'),
            url='/#about',
        ),
        MenuItem(
            title=_('FAQ'),
            url='/#faq',
        ),
        MenuItem(
            title=_('Contact Us'),
            url='/#contacts',
            item_id='contacts_%s' % prefix
        ),
    )

    return menu


def menu_footer(request):
    menu = Menu()
    prefix = 'footer'

    menu.append(
        MenuItem(
            title=_('Services'),
            url='/#services',
        ),
        MenuItem(
            title=_('Featured Property'),
            url='/#featured-properties',
        ),
        MenuItem(
            title=_('About Us'),
            url='/#about',
        ),
        MenuItem(
            title=_('FAQ'),
            url='/#faq',
        ),
        MenuItem(
            title=_('Contact Us'),
            url='/#contacts',
            item_id='contacts_%s' % prefix
        ),
    )

    return menu

