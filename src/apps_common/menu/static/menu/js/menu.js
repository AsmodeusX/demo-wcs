(function($) {
    'use strict';

    /*
        Класс, отвечающий за показ/скрытие меню при нажатии кнопки.

        Требует:
            jquery.utils.js

        Параметры:
            menuSelector        - селектор элемента меню
            buttonSelector      - селектор кнопки, активирующей меню
            openedClass         - класс body, когда меню открыто
            fullHeight          - должно ли меню быть на всю высоту

        События:
            // Перед показом мобильного меню
            before_open

            // Перед скрытием мобильного меню
            before_close

            // Изменение размера окна
            resize
    */

    var MainMenu = Class(EventedObject, function MainMenu(cls, superclass) {
        cls.init = function(options) {
            superclass.init.call(this);

            this.opts = $.extend({
                menuSelector: '.mobile-menu',
                buttonSelector: '.mobile-menu-button',
                openedClass: 'main-menu-opened',
                rootEl: 'html',

                fullHeight: false
            }, options);

            // клик на кнопку
            var that = this;
            $(document).off('.menu').on('click.menu', this.opts.buttonSelector, function() {
                if (that.isOpened()) {
                    return that.close();
                } else {
                    return that.open();
                }
            });
        };

        cls.getMenu = function() {
            return $(this.opts.menuSelector).first();
        };

        cls.isOpened = function() {
            return $(this.opts.rootEl).hasClass(this.opts.openedClass);
        };

        cls._open = function() {
            return $(this.opts.rootEl).addClass(this.opts.openedClass);
        };

        cls._close = function() {
            return $(this.opts.rootEl).removeClass(this.opts.openedClass);
        };

        cls.update = function() {
            var $menu = this.getMenu();
            if (!$menu.length) {
                return false;
            }

            // на всю высоту окна
            if (this.opts.fullHeight) {
                $menu.height('auto');
                var default_height = $menu.outerHeight();
                $menu.outerHeight(Math.max(default_height, $.winHeight()));
            }
        };

        /*
            Показ меню
         */
        cls.open = function() {
            var $menu = this.getMenu();
            if (!$menu.length) {
                return false;
            }

            if (this.trigger('before_open') === false) {
                return false;
            }

            this.update();
            this._open();
        };

        /*
            Скрытие меню
         */
        cls.close = function() {
            var $menu = this.getMenu();
            if (!$menu.length) {
                return false;
            }

            if (this.trigger('before_close') === false) {
                return false;
            }

            this._close();
        };
    });


    $(document).ready(function() {
        window.main_menu = MainMenu({
            fullHeight: false
        }).on('resize', function(winWidth) {
            // скрытие на больших экранах
            if (winWidth >= 768) {
                this.close();
            }
        });
    });

    $(window).on('resize.menu', $.rared(function() {
        if (!window.main_menu) {
            return
        }

        window.main_menu.update();
        window.main_menu.trigger('resize', $.winWidth());
    }, 100));

    $(window).on('load', function () {
        var w = $.winWidth();
        if (w < 1024) {
            var $mobileMenu = $('.mobile-menu'),
                $toggleButtons = $mobileMenu.find('.menu__toggle');

            // $('.scrollable').css('max-height', $mobileMenu.innerHeight() - $('.menu__bottom').innerHeight() - $('.mobile-menu .logo-box').innerHeight() - 80);

            $mobileMenu.find('.menu-inner').slideToggle();
            $toggleButtons.on('click', function (e) {
                e.preventDefault();
                var $toggleButton = $(e.target);
                var $item = $($toggleButton).closest('.general-item');
                var $innerMenu = $item.find('.menu-inner');
                $innerMenu.slideToggle();
                $toggleButton.toggleClass('opened');
            });
        }
    });
    $(document).on('scroll', checkSection);

    $(document).on('click', '.main-menu-opened', function (e){ // событие клика по веб-документу
        var div = $(".mobile-menu"); // тут указываем ID элемента
        // var header = $("header"); // тут указываем ID элемента
        // if (!($(e.target).hasClass('mobile-menu'))) {
        if (!div.is(e.target) // если клик был не по нашему блоку
            // && !header.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) {// если клик был не по нашему блоку
            // && header.has(e.target).length === 0) { // и не по его дочерним элементам
            window.main_menu.close();
        }
        // }

    });

    $(document).on('click', '.menu__item', function (e) {
        var is_main_page = window.location.pathname === '/';
        setTimeout(function () {
            var hash = window.location.hash.toLowerCase(); //Нашли все что в хэше
            if (is_main_page) {
                checkHash(hash);
            }
        })
    })

    function checkHash(hash) {
        var format_hash = hash.replace('#', ''); // Убрали #, оставили только название блока
        if (!!format_hash) { //Если есть название блока
            var $elem = $('.block--' + format_hash).eq(0); // То пытаемся найти первый блок с подходящим названием
            if ($elem && $elem.length > 0) { // Если блок найден
                $.scrollTo($elem.offset().top - $('.header--fixed').innerHeight() + 1, 2000); //То скроллим до него - высота фиксированного хэдера
                window.main_menu.close();
            }
        }
    }
    function checkSection() {
        var $menuItems = $('.menu__item');

        if ($menuItems && $menuItems.length > 0) {
            var $headerFixed = $('.header--fixed').innerHeight();
            var $sections = $('section.block');

            $sections.each(function () {
                var $block = $(this),
                    top = $block.offset().top - $headerFixed,
                    bottom = top + $block.innerHeight() - $headerFixed;

                if ($(document).scrollTop() >= top && $(document).scrollTop() <= bottom) {
                    var hash = $block.data('hash');

                    var $items = $(".menu__item");

                    $items.removeClass('active');

                    $items.each(function () {
                        var $item = $(this);
                        var $link = $item.find('a');
                        if ($link.attr('href') === '/#' + hash) {
                            // if (!$item.hasClass('active')) {
                                $item.addClass('active');
                                // window.location.hash = "#" + hash
                            // }
                        }
                    })
                }

            })
        }
    }

})(jQuery);
