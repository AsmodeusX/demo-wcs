from django.template import loader
from libs import jinja2


@jinja2.extension
class MenuExtension(jinja2.Extension):
    tags = {'menu'}
    takes_context = True

    def _menu(self, context, name, classes='', item_id_mod=None, template='menu/menu.html'):
        request = context.get('request')

        if not request:
            return ''

        menus = getattr(request, '_menus', None)
        if not menus:
            return ''
        return loader.render_to_string(template, {
            'level': 1,
            'item_id_mod': item_id_mod,
            'classes': '%s %s' % (name.replace('menu_', ''), classes),
            'items': menus.get(name, ()),
        }, request=context.get('request'))
