from django.template import loader
from django.utils.timezone import datetime
from django.conf import settings
from libs import jinja2
from .. import conf


@jinja2.extension
class FooterExtension(jinja2.Extension):
    tags = {'footer', 'dl_link'}
    takes_context = True

    def _footer(self, context, template='footer/footer.html'):
        # contacts = ContactsConfig.get_solo()
        str_copyright = "{year} {project}. All rights reserved.".format(year=datetime.now().year, project=settings.PROJECT_NAME)
        return loader.render_to_string(template, {
            'str_copyright': str_copyright,
            # 'contacts': contacts,
        }, request=context.get('request'))

    def _dl_link(self, context, template='footer/dl_link.html'):
        request = context.get('request')
        if not request:
            return ''

        rule = conf.RULES.get(request.path_info)
        if rule:
            return loader.render_to_string(template, rule, request=request)

        return loader.render_to_string(template, {
            'url': 'https://directlinedev.com/',
            'title': 'Web Development',
            'fallback': True,
        }, request=request)
