from django.template import loader
from libs import jinja2


@jinja2.extension
class HeaderExtension(jinja2.Extension):
    tags = {'header'}
    takes_context = True

    def _header(self, context, template='header/header.html', classes=None, item_id_mod=None, clickable=False):

        return loader.render_to_string(template, {
            'clickable': clickable,
            'classes': classes,
            'item_id_mod': item_id_mod,
            'is_main_page': context.get('is_main_page'),
        }, request=context.get('request'))
