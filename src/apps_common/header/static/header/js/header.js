(function ($) {
    // var clHeaderFixed = 'visible';
    var $headerFixed;

    $('document').ready(function () {
        $headerFixed = $('header.header--fixed');
        // $('.btn-contact-scroll').each(function () {
        //     var $link = $(this);
        //     var blockId = $link.attr('href').replace('#', '');
        //
        //     $link.on('click', function(e) {
        //         e.preventDefault();
        //         window.location.href = '#' + blockId;
        //         $.scrollTo($('.block--' + blockId).eq(0).offset().top - $('.header--fixed').innerHeight(), 1000);
        //
        //         return false;
        //     });
        // });

        var is_main_page = window.location.pathname === '/';

        $('.logo').on('click', function (e) {
            e.preventDefault();
            if (is_main_page) {
                $.scrollTo(0, 1000);
            } else {
                window.location.href = $(e.currentTarget).attr('href');
            }
        })
    })

    // $.visibilityInspector.inspect('header:not(.header--fixed)', {
    //     top: 0,
    //     bottom: 30,
    //     afterCheck: function($elem, opts, state) {
    //         if ($headerFixed) {
    //             if (state) {
    //                 $headerFixed.removeClass(clHeaderFixed);
    //             } else {
    //                 $headerFixed.addClass(clHeaderFixed);
    //             }
    //         }
    //     }
    // });
})(jQuery)
