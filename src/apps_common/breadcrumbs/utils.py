from django.utils.translation import ugettext_lazy as _
from main.models import MainPageConfig


# Чтобы везде не импортировать вручную мейн
def breadcrumb_main(request):
    conf = MainPageConfig.get_solo()
    request.breadcrumbs.add(_('Home'), conf.get_absolute_url())
