from ._pipeline import PIPELINE


PIPELINE['STYLESHEETS'].update({
    'critical': {
        'source_filenames': (
            'scss/grid.scss',

            'fonts/Poppins/600/normal/font.css',
            'fonts/Poppins/700/normal/font.css',
            'fonts/OpenSans/400/normal/font.css',

            'scss/layout.scss',
            'header/scss/header.scss',
            'webpage/scss/header.scss',
            'scss/components/logo/logo.scss',
            'scss/buttons.scss',
            'breadcrumbs/scss/breadcrumbs.scss',
            'menu/scss/menu.scss',
        ),
        'output_filename': 'css_build/critical.css',
    },
    'core': {
        'source_filenames': (
            'scss/forms.scss',
            'scss/preloader.scss',
            'css/swiper.min.css',
            'scss/swiper.scss',
            'scss/text_styles.scss',
            'social_networks/scss/social_links.scss',

            'blocks/scss/advantages.scss',
            'blocks/scss/about.scss',
            'blocks/scss/faq.scss',
            'blocks/scss/services.scss',
            'blocks/scss/popups/service.scss',

            'properties/scss/block.scss',

            'contacts/scss/block.scss',
            # 'scss/text_styles.scss',
            # 'webpage/scss/textpage_info.scss',
            'scss/popups/popups.scss',
            'scss/popups/preloader.scss',
            'footer/scss/footer.scss',
            'css/jquery.mCustomScrollbar.css',
        ),
        'output_filename': 'css_build/head_core.css',
    },
    'fonts': {
        'source_filenames': (
            'fonts/Poppins/300/normal/font.css',
            'fonts/Poppins/400/normal/font.css',
            'fonts/Roboto/100/normal/font.css',
            'fonts/Roboto/400/normal/font.css',
            'fonts/Roboto/700/normal/font.css',
            'fonts/OpenSans/700/normal/font.css',
        ),
        'output_filename': 'css_build/fonts.css',
    },
    'error_critical': {
        'source_filenames': (
            'scss/error_page.scss',
        ),
        'output_filename': 'css_build/error_critical.css',
    },
    'main': {
        'source_filenames': (
            'main/scss/index.scss',
        ),
        'output_filename': 'css_build/main.css',
    },
    'property_critical': {
        'source_filenames': (
            'scss/components/gallery/gallery.scss',
            'properties/scss/property.scss',
        ),
        'output_filename': 'css_build/property_critical.css',
    },
    # 'property': {
    #     'source_filenames': (
    #         'scss/components/gallery/gallery.scss',
    #         'properties/scss/property.scss',
    #     ),
    #     'output_filename': 'css_build/property.css',
    # },
    'success_critical': {
        'source_filenames': (
            'contacts/scss/success.scss',
        ),
        'output_filename': 'css_build/success_critical.css',
    },
})

PIPELINE['JAVASCRIPT'].update({
    'core': {
        'source_filenames': (
            'polyfills/modernizr.js',
            'js/jquery-3.3.1.min.js',
            'js/translate.js',
            'js/jquery-ui.min.js',

            'common/js/js.cookie.min.js',
            'common/js/jquery.utils.js',
            'js/jquery.inspectors.js',

            'sentry/js/sentry.bundle.tracing.min.js',
            'sentry/js/sentry.js',

            'js/TweenMax.min.js',
            'webpage/js/header.js',
            'header/js/header.js',
            'js/swiper.min.js',
            'js/jquery.fitvids.js',
            'js/jquery.youtube.js',
            'js/text_styles.js',
            'js/go_form.js',

            'js/popups/jquery.popups.js',
            'js/popups/preloader.js',
            'js/jquery.scrollTo.js',
            # 'webpage/js/share.js',

            'attachable_blocks/js/async_blocks.js',
            'menu/js/menu.js',
            'js/jquery.mCustomScrollbar.js',

            'blocks/js/advantages.js',
            'js/expander.js',
            'blocks/js/about.js',
            'blocks/js/faq.js',
            'blocks/js/services.js',
            'properties/js/block.js',

            'contacts/js/forms.js',

            'js/recaptcha.js',
        ),
        'output_filename': 'js_build/core.js',
    },
    'main': {
        'source_filenames': (
            'main/js/index.js',
        ),
        'output_filename': 'js_build/main.js',
    },
    'property': {
        'source_filenames': (
            'js/components/gallery/gallery.js',
            'properties/js/property.js',
        ),
        'output_filename': 'js_build/property.js',
    },
})

