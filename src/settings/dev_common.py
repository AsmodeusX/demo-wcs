from settings.common import *

DOMAIN = '.local.com'
SESSION_COOKIE_DOMAIN = DOMAIN
CSRF_COOKIE_DOMAIN = DOMAIN
ALLOWED_HOSTS = (
    DOMAIN,
    'localhost',
    '127.0.0.1',
)

DEBUG = True

ROOT_URLCONF = 'urls.dev_common'

DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'project',
        'USER': 'project',
        'PASSWORD': 'password',
        'HOST': 'localhost',
    }
})

# Нарезка в два потока (на продакшене для этого памяти не хватит)
VARIATION_THREADS = 2

# Отключение компрессии SASS (иначе теряется наглядность кода)
PIPELINE['SASS_ARGUMENTS'] = '-t nested'

STATICFILES_FINDERS += (
    'libs.pipeline.debug_finder.PipelineFinder',
)

# =========
#  Logging
# =========
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            'datefmt': '%d-%m-%Y %H:%M:%S',
            'format': '[%(asctime)s] %(levelname)-8s %(name)-12s %(message)s',
        },
        'server': {
            '()': 'libs.logging.formatters.SQLFormatter',
            'datefmt': '%d-%m-%Y %H:%M:%S',
            'format': '[%(asctime)s] %(message)s',
        },
        'file': {
            '()': 'libs.logging.formatters.UTCFormatter',
            'datefmt': '%d-%m-%Y %H:%M:%S',
            'format': '[%(asctime)s] %(levelname)-8s %(name)s\n'
                      '%(message)s'
        },
    },
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console',
        },
        'server': {
            'class': 'logging.StreamHandler',
            'formatter': 'server',
        },
        'debug_log': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.abspath(os.path.join(BASE_DIR, os.pardir, 'logs', 'debug.log')),
            'maxBytes': 1024*1024,
            'backupCount': 5,
            'formatter': 'file',
        },
        'errors_log': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.abspath(os.path.join(BASE_DIR, os.pardir, 'logs', 'errors.log')),
            'maxBytes': 1024*1024,
            'backupCount': 2,
            'level': 'ERROR',
            'formatter': 'file',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['null'],
        },
        'django.db': {              # лог SQL-запросов
            'handlers': ['null'],
            'propagate': False,
            'level': 'DEBUG',
        },
        'django.server': {
            'handlers': ['server'],
            'propagate': False,
        },
        'debug': {
            'handlers': ['console', 'debug_log'],
            'propagate': False,
            'level': 'DEBUG',
        },
        '': {
            'handlers': ['console', 'errors_log'],
            'level': 'INFO',
        }
    }
}
