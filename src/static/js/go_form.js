(function ($) {
    $(document).on('click', '.go-contact', function (e) {
        e.preventDefault;
        var current_popup = getCurrentPopup();
        if (current_popup) {
            current_popup.destroy()
        }
        $('.menu__item > a[href="/#contacts"]').eq(0).trigger('click');
    })
})(jQuery);