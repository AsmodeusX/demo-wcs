(function ($) {
    var clVisible = 'language--visible';

    var checkActiveLanguage = function () {
        var current_language = gt_get_cookie('googtrans').split('/').pop();
        return current_language !== 'es' ? 'en' : 'es';
    }

    $(window).on('load', function() {
        var currentLang = checkActiveLanguage();
        var translate = formatTranslate(currentLang);
        var $langs = $('.language');

        $langs.each(function () {
            var $lang = $(this);

            $lang.on('click', function (e) {
                e.preventDefault();
                $langs.addClass(clVisible);
                $lang.removeClass(clVisible);

                currentLang = $lang.data('lang');
                translate = formatTranslate(currentLang);
                doGTranslate(translate);
            });
        })

        if (currentLang === 'es')
            $('.language--es').trigger('click');
        else {
            $('.language--es').addClass(clVisible);
        }
    });


    function formatTranslate(lang) {
        var translate = 'en|en';

        switch (lang) {
            case 'en':
                translate = 'en|en'
                break
            case 'es':
                translate = 'en|es'
                break
        }

        return translate;
    }

    function gt_get_cookie(cname) {
        var name = cname + "=",
            decodedCookie = decodeURIComponent(document.cookie),
            ca = decodedCookie.split(';');

        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
})(jQuery);

