(function ($) {

    /*
    * Скрипт для обработки форм
    * Разово загружает капчу, отрабатывает при отправке форм
    * после проверки капчи вызывает отправку, которая может быть
    * глобальной и расписанной в отдельном скрипте.
    *
    * Пример:
    *
    * */

    var loaded = false, tf;

    window.form_send = false;

    window.recaptchaCallback = function() {
        var captchaOptions = {
            sitekey: document.documentElement.getAttribute('data-google-recaptcha-key'),
            size: 'invisible',
            callback: window.fnSend
        };


        if ($('#recaptcha_contact').length) {
            window.recaptcha_contact = window.grecaptcha.render('recaptcha_contact', captchaOptions);
        }
    };

    $(document).on('submit', '#fmContact', function (e) {
        e.preventDefault();

        window.form = $('#fmContact');

        tf = 'contact';

        if (!window.form_send) {
            var token = window.grecaptcha.getResponse(window.recaptcha_contact);
            if (!token) {
                window.grecaptcha.execute(window.recaptcha_contact);
                return false;
            }
        }
    });

    $(window).one('mousemove scroll keydown touchstart', function (e) {
        if (!loaded) {
            get_captcha();

            loaded = !loaded;
        }
    });

    function get_captcha() {
        $.getScript('https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&render=explicit&hl=en');
    }
})(jQuery);