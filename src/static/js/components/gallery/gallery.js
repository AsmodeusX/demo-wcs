(function($) {
    /** @namespace gettext */

    $(window).on('load', function () {
        var $galleries = $('.gallery');

        $galleries.each(function () {
            var $gallery = $(this);
            var previews = new Swiper($gallery.find('.gallery__list-gallery').get(0), {
                spaceBetween: 10,
                slidesPerView: 5,
                direction: 'vertical',
                freeMode: true,
                // height: 600,
                watchSlidesProgress: true,
                scrollbar: {
                    el: $gallery.find(".swiper-scrollbar").get(0),
                    draggable: true
                },
                mousewheel: true
            });

            var swiper = new Swiper($gallery.find('.gallery__current-slide').get(0), {
                spaceBetween: 0,
                thumbs: {
                    swiper: previews
                },
                navigation: {
                    nextEl: $gallery.find('div.swiper-button-next').get(0),
                    prevEl: $gallery.find('div.swiper-button-prev').get(0)
                },
                pagination: {
                    el: $gallery.find(".swiper-pagination").get(0),
                    type: "progressbar"
                }

            });
        });

    });
})(jQuery);
